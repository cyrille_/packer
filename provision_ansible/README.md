Objectif : Construire ses propres rôles pour déployer un wordpress sous docker
Travail d'équipe : oui
Besoin :

    Création d'un serveur sous debian 11 avec vagrant
    Création des rôles ansible suivants :
        common : ce rôle doit regrouper toutes vos pratiques d'installation par défaut (création d'utilisateur, mise à jour système, paramétrage de la timezone, hostname...)
        docker : ce rôle doit vous permettre l'installation de docker et docker-compose
        dck-wordpress : ce rôle vous permet de déployer un wordpress sous docker (penser à utiliser le docker-compose du précédent TP)
    L'ensemble des rôles peuvent être appelé par le playbook principale main.yml
    Essayer d'utiliser le provisionner ansible local pour vous permettre d'exécuter les playbooks directement sur la machine guest

Rendu :

    Versionner votre code dans un projet GitLab
    Documenter son usage dans un document README.md# TP_Ansible

