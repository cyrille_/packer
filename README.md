# Packer

## Projet

Création d'une instance sur AWS avec Ubuntu Xenial, installation de ansible-local afin de jouer les playbooks suivant : 


- Rôle 1 : gestion des dossiers, fichiers et utilisateurs, paramètrages etc. 
- Rôle 2 : Installation de Docker et Docker Compose
- Rôle 3 : Créer un Wordpress à l'aide de Docker Compose

## Etape 1

- Configuration des credentials amazon :

```
export AWS_ACCESS_KEY_ID=YOUR_ACCESS_KEY$ 
export AWS_SECRET_ACCESS_KEY=YOUR_SECRET_KEY
```

- Initialisation de Packer :

```bash
packer init .
```

Formater et valider le template :

```bash
packer fmt .
```
```bash
packer validate .
```

## Etape 2

Configuration du template .hcl 

https://learn.hashicorp.com/tutorials/packer/aws-get-started-build-image?in=packer/aws-get-started


## Etape 3

Créer la partie ansible rôle + tâches

https://groupe3.cefim-formation.org/cyrillecefim/tp_ansible/-/blob/main/README.md

## Lancement

```bash
packer build aws-ubuntu.pkr.hcl
```

## ToDo

Les 3 rôles se jouent. 

Petit bémol dans la tâche common : la mise à jour a été annulée, car il faut utiliser un autre module plus sécurisé. 

## Complément :

Ici, dans le projet Packer, les rôles ont été adaptés pour fonctionné sous Ubuntu. La version des rôles dans le projet "tp_ansible" est une version configurée pour Debian 11. 

